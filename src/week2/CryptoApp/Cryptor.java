package week2.CryptoApp;

import java.util.HashMap;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> conversionMap = new HashMap<>();


    public String convert(String inputText) {
        String result = "";
        char[] inputTextCharacters = inputText.toCharArray();
        for (int i = 0; i < inputTextCharacters.length; i++) {
            result = result + this.conversionMap.get(String.valueOf(inputTextCharacters[i]));
        }


        return result;

    }
}