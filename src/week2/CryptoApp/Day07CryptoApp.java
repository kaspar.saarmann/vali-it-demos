package week2.CryptoApp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Day07CryptoApp {
    public static void main(String[] args) throws IOException {
        List<String> alphabet = Files.readAllLines(Paths.get(args[0])); //loeme aplhabet.txt faili sisse

        Scanner scanner = new Scanner (System.in);

        Cryptor encryptor = new Encryptor(alphabet);
        Cryptor decryptor = new Decryptor(alphabet);

        System.out.println("Krüptoäpp");
        while (true){
            System.out.println("Mida soovid teha? [1 - välju, 2 - krüpteeri, 3 - dekrüpteeri]: ");
            String command = scanner.nextLine();
            switch (command){
                case "1":
                    System.out.println("Head aega");
                    return;
                case "2" :
                    System.out.println("Sisesta krüpteerimist vajav tekst: ");
                    String text = scanner.nextLine();
                    System.out.println("Krüpteeritud tekst: " + encryptor.convert(text));
                    break;
                case "3" :
                    System.out.println("Sisesta dekrüpteerimist vajav tekst: ");
                    String text1 = scanner.nextLine();
                    System.out.println("Dekrüpteeritud tekst: " + decryptor.convert(text1));
                    break;
                default:
                    System.out.println("Tundmatu käsklus!");
                    break;
            }
        }

//        System.out.println("Krüpteerimist vajav sõnum: " + args[1]);
//        String encryptedMessage = encryptor.convert(args[1]);
//        System.out.println("Dekrüpteerimist vajav sõnum: " + encryptedMessage);
//        String decryptedMessage = decryptor.convert(encryptedMessage);
//        System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);

//        Map<String, String> enryptionMap = new HashMap<>();
//        Map<String, String> decryptMap = new HashMap<>();
//        for (int i = 0; i < alphabet.size(); i++) {
//            String[] wordPair = alphabet.get(i).split(", ");
//            enryptionMap.put(wordPair[0], wordPair[1]);
//            decryptMap.put(wordPair[1], wordPair[0]);
//        }
//        System.out.println("Krüpteerimist vajav sõnum: " + args[1]);
//        String encryptText = convert(args[1], enryptionMap);
//        System.out.println("Krüpteeritud sõnum: " + encryptText);
//        System.out.println("Dekrüpteeritud sõnu: " + convert(encryptText, decryptMap));

    }


}

