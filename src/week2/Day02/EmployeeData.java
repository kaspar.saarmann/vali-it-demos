package week2.Day02;

public class EmployeeData {
    public String firstName;
    public String lastName;
    public int age;
    public String profession;
    public String citizenship;

    public EmployeeData(String data){ // konstruktor
        String[] dataParts = data.split(", ");
        this.firstName = dataParts[0].split(": ")[1];
        this.lastName = dataParts[1].split(": ")[1];
        this.age = Integer.parseInt(dataParts[2].split(": ")[1]);
        this.profession = dataParts[3].split(": ")[1];
        this.citizenship = dataParts[4].split(": ")[1];


    }
    @Override
    public String toString(){ // vajalik selleks et println väljakutsudes kutsuks selle välja, vastasel korral println funktsioon trükib välja selle mäluaadressi
        return String.format("Nimi: %s %s / vanus: %s / amet: %s / kodakonsus: %s\n", this.firstName, this.lastName, this.age, this.profession, this.citizenship);

    }

}
