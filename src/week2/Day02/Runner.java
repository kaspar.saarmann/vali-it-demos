package week2.Day02;

import week2.Day02.Athlete;

public class Runner extends Athlete {

    public Runner(String firstName, String lastName, int age, String gender, int height, int weight) { //pean tegema sama konsturkori
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String getAthleteType() {
        return "Runner";
    }

    @Override
    public String perform() {
        return "Running...";

    }
}
