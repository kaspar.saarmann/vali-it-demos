package week2.Day02;

public class Employee {
    public String firstName;
    public String lastName;
    public String position;

    public void printEmployeeDetails(){
//           this.lastName // this on keyword enda property leidmiseks
        System.out.printf("Employee details: %s %s / working as: %s", this.firstName, this.lastName, this.position);

    }
}
