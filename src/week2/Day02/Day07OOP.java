package week2.Day02;

public class Day07OOP {
    public static void main(String[] args) {

        Athlete skydiver1 = new Skydiver("Teet", "Hyppaja", 33, "M", 173, 65);
        Athlete skydiver2 = new Skydiver("Pille", "Maasikas", 22, "F", 164, 45);
        Athlete skydiver3 = new Skydiver("Juku", "Pihlakas", 45, "M", 199, 59);
        Athlete runner1 = new Runner("Raud", "Polt", 34, "M", 454, 34);
        Athlete runner2 = new Runner("Auto", "Ratas", 134, "M", 344, 33);
        Athlete runner3 = new Runner("Vihma", "Vari", 345, "F", 45, 13);

//        System.out.println("Skydiver 1 first name: " + skydiver1.getFirstName() + " / lastname: " + skydiver1.getLastName() + " / Gender: " + skydiver1.getGender() + " / height: " + skydiver1.getHeight() + " cm" + " / weight: " + skydiver1.getWeight());
//        System.out.println("Skydiver 2 first name: " + skydiver2.getFirstName() + " / lastname: " + skydiver2.getLastName() + " / Gender: " + skydiver2.getGender() + " / height: " + skydiver2.getHeight() + " cm" + " / weight: " + skydiver2.getWeight());
//        System.out.println("Skydiver 3 first name: " + skydiver3.getFirstName() + " / lastname: " + skydiver3.getLastName() + " / Gender: " + skydiver3.getGender() + " / height: " + skydiver3.getHeight() + " cm" + " / weight: " + skydiver3.getWeight());
//
//        System.out.println("Skydiver 1 first name: " + skydiver1.getFirstName());
//        System.out.println("Skydiver 1 last name: " + skydiver1.getLastName());
//        System.out.println("Skydiver 1 gender: " + skydiver1.getGender());
//        System.out.println("Skydiver 1 height " + skydiver1.getHeight());
//        System.out.println("Skydiver 1 weight: " + skydiver1.getWeight());
//        System.out.println("Skydiver 2 first name: " + skydiver2.getFirstName());
//        System.out.println("Skydiver 2 last name: " + skydiver2.getLastName());
//        System.out.println("Skydiver 2 gender: " + skydiver2.getGender());
//        System.out.println("Skydiver 2 height " + skydiver2.getHeight());
//        System.out.println("Skydiver 2 weight: " + skydiver2.getWeight());
//        System.out.println("Runner 1 first name: " + runner1.getFirstName());
//        System.out.println("Runner 1 last name: " + runner1.getLastName());
//        System.out.println("Runner 1 gender: " + runner1.getGender());
//        System.out.println("Runner 1 height " + runner1.getHeight());
//        System.out.println("Runner 1 weight: " + runner1.getWeight());

          skydiver1.introduce();
          skydiver2.introduce();
          runner1.introduce();



          Dog thaiDog = new ThaiDog();
          Dog malayDog = new MalayDog();
          Dog estoniaDog = new EstoniaDog();

        System.out.println("ThaiDog bark: ");
        thaiDog.bark();
        System.out.println("malayDog bark: ");
        malayDog.bark();
        System.out.println("EstoniaDog bark: ");
        estoniaDog.bark();




          }

    }



