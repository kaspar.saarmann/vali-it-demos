package week2.Day02;

import week2.Day02.Athlete;

public class Skydiver extends Athlete {

    public Skydiver(String firstName, String lastName, int age, String gender, int height, int weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String getAthleteType() {
        return "Skydiver";
    }

    @Override
    public String perform() {
        return "Falling from sky...";

    }
}
