package week2.Day02;

import java.util.*;
import java.util.stream.Collectors;

public class AddEx {
    public static void main(String[] args) {
       String personStr = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA;";
       String[] persons = personStr.split("; ");
       String [][] personsData = new String[persons.length][5]; // Stringsi person on 5 elementi - eestnimi, perenimi, vanus, amet, kodakonsus
       for(int personIndex = 0; personIndex < personsData.length; personIndex++) {
           String[] personDetails = persons[personIndex].split(", ");
           for (int personProperty = 0; personProperty < 5; personProperty++){
               personsData[personIndex][personProperty] = personDetails[personProperty].split( ": ")[1];

//               personsData[personIndex] = personDetails;

           }
       }

       for(int i = 0; i< personsData.length; i++){
           System.out.printf("Nimi: %s %s / vanus: %s / amet: %s / kodakondsus: %s\n", personsData[i][0], personsData[i][1], personsData[i][2], personsData[i][3], personsData[i][4]);

       }

        //sama ülesanne listidega
        List<List<String>> personsList = Arrays.asList((personStr + " ").split("; "))
                .stream().map(p -> {
                    return Arrays.asList(p.split(", "))
                            .stream().map(v -> v.split(": ")[1]).collect(Collectors.toList());
                }).collect(Collectors.toList());


        // Lists klassikalisel viisil
        List<String> personStrList = Arrays.asList((personStr + " ").split("; "));
        List<List<String>> personList2 = new ArrayList<>();
        for (int i = 0; i < personStrList.size(); i++){
            String[] personProperties = personStrList.get(i).split(", ");
            List<String> personPropertiesList = new ArrayList<>();
                for(int j = 0; j < personProperties.length; j++) {
                    personPropertiesList.add(personProperties[j].split(": ")[1]);

                }
                    personList2.add(personPropertiesList);

        }
        //Maps
        //List<Map<String, String>>
        List<String> personStrList2 = Arrays.asList((personStr + " ").split("; "));
        List<Map<String, String>> personsListOfMaps = new ArrayList<>();
        for(int i = 0; i < personStrList2.size(); i++) {
            String[] personProperties = personStrList.get(i).split(", ");
            Map<String, String> personsMap = new HashMap<>();
            for (int j = 0; j < personProperties.length; j++){
                String [] personPropArr = personProperties[j].split(": ");
                personsMap.put(personPropArr[0], personPropArr[1]);

            }
        }
        //Custom class (EmployeeData) - teeme andmete hoidmiseks objekt
        String[] listOfEmployeeStrings = (personStr + " ").split("; ");
        List<EmployeeData> listOfEmployees = new ArrayList<>();
        for(int i = 0; i < listOfEmployeeStrings.length; i++ ){
            listOfEmployees.add(new EmployeeData(listOfEmployeeStrings[i]));

        }

        for(int j = 0; j < listOfEmployees.size(); j++){
            System.out.println(listOfEmployees.get(j));
        }


    }



}
