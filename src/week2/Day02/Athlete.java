package week2.Day02;
// ülesanne 1
public abstract class Athlete {

    public abstract String perform();

    private String firstName;
    private String lastName;
    private int age;
    private String gender;
    private int height;
    private int weight;

    public abstract String getAthleteType();


    public void introduce(){
        System.out.printf("----- %s -----\n", this.getAthleteType());
        System.out.printf("Name %s %s\n", this.getFirstName(), this.getLastName());
        System.out.printf("Age: %s, Weight; %s, Height: %s\n", this.getAge(), this.getWeight(), this.getHeight());
        System.out.printf("Age: %s\n", this.getGender());

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public Athlete(String firstName, String lastName, int age, String gender, int height, int weight) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Iam an Athlete and I can to this...." + this.perform();
    }


}
