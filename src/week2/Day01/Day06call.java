package week2.Day01;

public class Day06call {
    public static void main(String[] args) {

        String code = "38605010298";
        boolean isCodeCorrect = Day06Ex.validatePersonalCode(code);
        System.out.println(isCodeCorrect);

        printNumber(23);

    }

    //Näide meetod mis kutsub ennast ise välja
    public static void printNumber (int upperLimit){
        System.out.println(upperLimit);
        if ( upperLimit > 1) {
            printNumber(--upperLimit);
        }
    }


}
