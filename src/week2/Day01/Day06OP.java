package week2.Day01;

import week2.Day02.Employee;

public class Day06OP {
    public String testProperty;


    public static void main(String[] args) {
        Day06OP day6obj = new Day06OP();

        Employee emp1 = new Employee();
        emp1.firstName = "Donald";
        emp1.lastName = "Trump";
        emp1.position = "pilot";

        Employee emp2 = new Employee();
        emp2.firstName = "Juku";
        emp2.lastName = "Muna";
        emp2.position = "Ehitaja";

        emp1.printEmployeeDetails();
        emp2.printEmployeeDetails();


    }


    public void testMethod(){
        System.out.println("Hello, OOP");

    }
}
