package week2.Day01;

import week2.Day02.CountryInfo;

public class Day06Ex2OOP {
    public static void main(String[] args) {

        CountryInfo estonia = new CountryInfo();
        estonia.countryName = "Eesti";
        estonia.capital = "Tallinn";
        estonia.primeMinister = "Jüri Ratas";
        estonia.languages = new String[]{"Eesti", "Soome", "Vene"};

        CountryInfo finland = new CountryInfo("Soome", "Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi"});
/*
        finland.countryName = "Soome";
        finland.capital = "Helsinki";
        finland.primeMinister = "Antti Rinne";
        finland.languages = new String[] {"Rootsi", "Soome"};
*/
        CountryInfo germany = new CountryInfo();

        CountryInfo[] countries = {estonia, finland, germany};

//        for (int i =0; i < countries.length; i++){
//            System.out.printf("Name %s, capital %s, primeminister %s: \n", countries[i].countryName, countries[i].capital,
//                    countries[i].primeMinister);
//            for (int j = 0; j < countries[i].languages.length; j++ ){
//                System.out.println("\t" + countries[i].languages[j]);
//            }
//
//        }
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);
        }
    }
}
