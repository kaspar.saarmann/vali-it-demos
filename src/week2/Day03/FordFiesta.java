package week2.Day03;

public class FordFiesta extends Car{

    @Override
    public String getCarMake() {
        return "Ford";
    }

    @Override
    public String getCarModel() {
        return "Fiesta";
    }

    @Override
    public int getMaxSpeed() {
        return 200;
    }
}
