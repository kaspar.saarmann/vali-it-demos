package week2.Day03;

public class Day8OOP {
    public static void main(String[] args) {

        Car myCar = new Car();
        System.out.println(myCar instanceof Movable);
        System.out.println(myCar instanceof Car);

        System.out.println();
        Car ferrari = new FerrariF355();
        System.out.println("Ferrari is Movable?");
        System.out.println(ferrari instanceof Movable);
        System.out.println("Ferrari is Car?");
        System.out.println(ferrari instanceof Car);
        System.out.println("Ferrari is Ford Fiesta?");
        System.out.println(ferrari instanceof FordFiesta);


    }
}
