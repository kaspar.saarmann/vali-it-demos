package week2.Day03;

public class RenaultMegane extends Car {

    @Override
    public String getCarMake() {
        return "Renault";
    }

    @Override
    public String getCarModel() {
        return "Megane";
    }

    @Override
    public int getMaxSpeed() {
        return 150;
    }
}


