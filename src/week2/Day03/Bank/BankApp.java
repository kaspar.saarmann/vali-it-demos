package week2.Day03.Bank;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) throws IOException {

        AccountService.loadAccounts(args[0]);


        //Account account = AccountService.search("653797240");

        Scanner scanner = new Scanner(System.in);
        System.out.println("SUPERPANK");
        while (true) {
            System.out.println("Sisesta käsklus: EXIT, BALANCE kontonumber või TRANSFER");
            String command = scanner.nextLine(); //BALANCE 656129320
            String[] commandParts = command.split(" ");

            switch (commandParts[0]) {
                case "EXIT":
                    System.out.println("Head aega");
                    return;
                case "BALANCE":
                    if(commandParts.length == 2) {
                        Account accountBalance = AccountService.search(commandParts[1]);
                        printAccount(accountBalance);
                    } else if (commandParts.length == 3){
                        Account accountBalance1 = AccountService.search(commandParts[1], commandParts[2]);
                        printAccount(accountBalance1);
                    } else
                System.out.println("Viga: ebakorrektne käsklus");
                break;
                case "TRANSFER": // käsk - TRANSFER 656129320 684545168 10
                    TransferResult result = AccountService.transfer(commandParts[1], commandParts[2], Double.parseDouble(commandParts[3]));
                    if (result.getResultMessage().equals("OK")){
                        System.out.println("Ülekanne õnnestus!");
                        System.out.println("Maksja konto: ");
                        printAccount(result.getFromAccount());
                        System.out.println("Saaja konto: ");
                        printAccount(result.getToAccount());
                    } else {
                        System.out.println("---------------");
                        System.out.println("Ülekanne ebaõnnestus");
                        System.out.println("Põhjus: ");
                        System.out.println(result.getResultMessage());
                    }

                    break;
                default:
                    System.out.println("Viga, tundmatu käsklus");
            }
        }
    }

    private static void printAccount(Account accountBalance) {
        if (accountBalance != null){
            System.out.println("---------------");
            System.out.println("Leiti konto:");
            System.out.println("Nimi: " + accountBalance.getFirstName() + " " + accountBalance.getLastName());
            System.out.println("Konto jääk: " + accountBalance.getBalance() + " eur");
            System.out.println("---------------");
        } else {
            System.out.println("Kontot ei leitud");
        }
    }
}
