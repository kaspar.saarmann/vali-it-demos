package week2.Day03.Bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {

    private static List<Account> customers = new ArrayList<>();

    public static void loadAccounts(String accountFilePath) throws IOException { //meetod millega kutsume välja failist lugemise
        List<String> accountStrings = Files.readAllLines(Paths.get(accountFilePath));
        for (int i = 0; i < accountStrings.size(); i++) {
            String accountText = accountStrings.get(i); // käime Listi läbi

            // teeme Jean, Waters, 758..., 2110 account objecti sest Stringi ei saa accountsi lisada
            Account account = new Account(accountText); // objekti loomine
            customers.add(account);
        }
    }

    public static Account search(String accountNumberToSearch) {
        for (int i = 0; i < customers.size(); i++) {
            // customers.get(i).getAccountNumber();

            if (customers.get(i).getAccountNumber().equals(accountNumberToSearch)) {
                return customers.get(i);

            }
        }

        return null;

    }


    public static Account search(String customerFirstName, String customerLastName) {
        for (int i = 0; i < customers.size(); i++) {
            if (customerFirstName.equals(customers.get(i).getFirstName()) && customerLastName.equals(customers.get(i).getLastName())) {
                return customers.get(i);

            }


        }
        return null;
    }

    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, double sum) {
        Account fromAccount = search(fromAccountNumber);
        Account toAccount = search(toAccountNumber);
        if (fromAccount != null && toAccount != null) { // kõigepealt kontroll kas kontod üldse eksisteerivad
            if (fromAccount.getBalance() >= sum) { // kas kontol on piisavalt raha
                toAccount.setBalance(toAccount.getBalance() + sum);
                fromAccount.setBalance(fromAccount.getBalance() - sum);
                return new TransferResult("OK", fromAccount, toAccount);
            } else {
                return new TransferResult("Kontol pole piisavalt vahendeid!", fromAccount, toAccount);

            }
        }else {
            return new TransferResult("Ei leitud kõiki kontosid!", fromAccount, toAccount);
        }

    }
}

