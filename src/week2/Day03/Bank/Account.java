package week2.Day03.Bank;

public class Account {

    private String firstName;
    private String lastName;
    private String accountNumber;
    private double balance;


    public Account (String accountData){ // loome tühja objekti hetkel mida pärast hakkame kasutama
        String[] parts = accountData.split(", "); // splitime txt failis koma järgi
        this.firstName = parts[0]; // Jean
        this.lastName = parts[1]; // Waters
        this.accountNumber = parts[2]; // 7588...
        this.balance = Double.parseDouble(parts[3]); // 2110.0

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
