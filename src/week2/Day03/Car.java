package week2.Day03;

public class Car implements Movable {
    public String color;

    public String getCarMake (){
        return "Opel";
    }

    public String getCarModel(){
        return "Astra";
    }

    public int getMaxSpeed(){
        return 180;
    }


    @Override
    public void startEngine() {
        System.out.println("Brrmmm!");
    }

    @Override
    public void speedUp(int amount) {
        System.out.println("Increase speed by " + amount + " km/h");
    }

    @Override
    public void slowDown(int amount) {
        System.out.println("Decrease speed by " + amount + " km/h");
    }

    @Override
    public int getFuelLeft() {
        return (int) (Math.random() * 70) + 1;
    }


}
