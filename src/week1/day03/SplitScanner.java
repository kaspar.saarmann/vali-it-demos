package week1.day03;

import java.util.Scanner;

public class SplitScanner {
    public static void main(String[] args) {

        //Split
        String names = "Maarika, Malle, Paul, Kalle"; // tahame eraldada
        // tahame eraldada ._ järel, teeme sellest massiivi
        String[] namesList = names.split(", ");

        //Scanner
//        Scanner scanner = new Scanner(System.in); // system.in on sama mis system out print aga sisestamiseks
//        System.out.println("palun sisesta oma nimi:");
//        String userName = scanner.nextLine();
//        System.out.println("Sinu nimi on " + userName);

        Scanner textProcessingScanner = new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");
        textProcessingScanner.useDelimiter("Rida: ");
//        System.out.println(textProcessingScanner.next());
//        System.out.println(textProcessingScanner.next());
//        System.out.println(textProcessingScanner.next());

        while (textProcessingScanner.hasNext()) {
            System.out.println(textProcessingScanner.next());
        }
    }
}
