package week1.day03;

import java.util.Scanner;

public class Loops {
    public static void main(String[] args) {

        for (int i = 0; i < 3; i++) {
            System.out.println("ok " + i);

        }
        //for (;;);
        // juhtmuutuja defineerimine ja suurendamine väljaspool tsükli päist.
        int num = 5;
        for (; num < 6; ) {
            System.out.println("Tsykkel: " + num);
            num++;

        }

        for (int u = 100; u > 0; u = u - 4 * 8) {
            System.out.println("U väärtus: " + u);
        }

        String[] employees = {"Rain", "Raiko", "Heidi", "Toomas"};
        for (int i = 0; i < employees.length; i++) {
            System.out.println("Töötaja: " + employees[i]);
        }

        //Foreach
        String[] employees2 = {"Rain", "Raiko", "Heidi", "Toomas"};
        for (String employee : employees2) {
            System.out.println("Töötaja nimi: " + employee);
        }

        //While
        String[] employees3 = {"Rain", "Raiko", "Heidi", "Toomas"};

        int index = 0;
        while (index < employees3.length) {
            System.out.println("Töötaja: " + employees3[index]);
            index++;
        }

        //do-while tsükkel - käib alati vähemalt ühekorra tsükli läbi
//        Scanner scanner = new Scanner(System.in);
//        int number = 0;
//        do {
//            System.out.println(("sisesta number, mis on suurem kui 678 ja jagub 3ga: "));
//            number = Integer.parseInt(scanner.nextLine());
//            if (number > 678 && number % 3 == 0);
//            System.out.println("Number sobib!");
//        } while (!(number > 678 && number % 3 == 0)); //number on suurem kui 678 ja samal ajal jaguks 3ga. ! keerab tsükli loogika teistpidi

        //Continue
        String[] employees4 = {"Rain", "Raiko", "Heidi", "Toomas"};
        for (String name: employees4) {
            if (name.startsWith("H")) {
                //tee midagi tarka
            } else {
                continue;
            }
            //tee midagi veel...
            //tee midagi veel...
        }

        //Break
        for(int i = 1; i < 100; i++){
            if(i % 42 == 0){  // kui i jagub 42ga.
                System.out.println("Fataalne tingimus: Tsükkel ei jätku");
                break;
            }
            //tee midagi
            System.out.println("Number " + i);
        }

    }
}
