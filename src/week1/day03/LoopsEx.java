package week1.day03;

import javax.swing.*;
import java.util.Arrays;

public class LoopsEx {
    public static void main(String[] args) {

        //Ex 11
        int i = 1;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

        //Ex 12
        for (i = 1; i <= 100; i++) {
            System.out.println(i);
        }

        //Ex13
        int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int digit : digits) {
            System.out.println(digit);

        }

        //Ex 14

        for (i = 3; i <= 100; i += 3) {
            System.out.println(i);
        }

        //Ex15
        //"text".length
        //"text.substring() // "tere".substring(1, 3); --> er
        String[] minuString = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String result = ""; // teeme väärtuse mille sisse lõpuks list tekib, teeme tühja stringi
        for (String minu : minuString){
            result += minu + ", "; // liidab tühjale stringile esimese bändi massiivist ja koma tühikuga
        }
        result = result.substring(0, result.length() - 2); // eemaldab viimased tühiku ja koma //substring prindib välja kõik elemendid 0 kuni kogupikkuse
        System.out.print(result);

        String result2 = String.join(", ", minuString);
        System.out.println(result2);

        //Ex 16
        String result3 = "";
        for (String minu : minuString){
            result3 = minu + ", " + result3;

        }
        result3 = result3.substring(0, result3.length() - 2 );
        System.out.println(result3);

        //Ex 17
        //Step 1 prindi välja args massiiv, iga parameeter eraldi real

        String numbersAsText = "";
        for (int y = 0; y < args.length; y++) {
            switch (args[y]) {
                case "0":
                    numbersAsText = numbersAsText + " " + " null";
                    break;
                case "1":
                    numbersAsText = numbersAsText + " üks";
                    break;
                case "2":
                    numbersAsText = numbersAsText + " kaks";
                    break;
                case "3":
                    numbersAsText = numbersAsText + " kolm";
                    break;
                case "4":
                    numbersAsText = numbersAsText + " neli";
                    break;
                case "5":
                    numbersAsText = numbersAsText + " viis";
                    break;
                case "6":
                    numbersAsText = numbersAsText + " kuus";
                    break;
                case "7":
                    numbersAsText = numbersAsText + " seitse";
                    break;
                case "8":
                    numbersAsText = numbersAsText + " kaheksa";
                    break;
                case "9":
                    numbersAsText = numbersAsText + " üheksa";
                    break;
                default:
                    System.out.println("Error");
            }

        }
        System.out.println(numbersAsText.substring(1, numbersAsText.length()));

        //Ex 18
        double randomNumber = 0;

        do {
            System.out.println("tere");
            randomNumber = Math.random();

        } while (randomNumber < 0.5);

        //Ex 19



    }
}
