package week1.day04;

import java.util.ArrayList;
import java.util.List;

class Day04Collections    {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Ahti");
        names.add("Ahto");
        names.add("Leemet");
        names.add("Linda");
        System.out.println(names);

        //vahetame Ahto - Reedaga, peab kasutama list.set meetodit
        names.set(1, "Reet");
        System.out.println(names);

        //listi vahele lisamine
        names.add(1, "Thomas"); // lisame 1 järele Thomase ja lükkab kõik ülejäänud edasi
        System.out.println(names);

        //Elemendi eemaldamine
        names.remove("Thomas");
        names.remove(0);    // eemaldame indeksiga
        System.out.println(names);

        //otsimine
        System.out.println("Kas sisaldab nime Linda? " + names.contains("Linda")); //annab true kui õige
        System.out.println("Kas sisaldab nime Juku? " + names.contains("Juku"));

        //mitmes element on Linda?
        System.out.println("Mitmes element on Linda? " + names.indexOf("Linda"));
        System.out.println("Mitmes element on Juku? " + names.indexOf("Juku")); //annab vastuseks -1

        //Asendame nime teisega, kõigepealt vaja teada mitmes element on, seejärel asendada

        names.forEach(System.out::println); // trükib listi elemendid omaette real
        names.forEach(t -> System.out.println("Töötaja nimi " + t)); // t on suvaline ajutine muutuja, kus iga tsükli tegemisel pannakse selle asemele järgmine element listist


    }
}
