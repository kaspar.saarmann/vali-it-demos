package week1.day04;

import javafx.util.Pair;

import java.sql.SQLOutput;
import java.util.*;
import java.util.concurrent.LinkedTransferQueue;

public class ExGameInsertNumber {
    private static final int RANGE = 10;

    public static void main(String[] args) {
        //List<Map<String, Integer>> board = new ArrayList<>();
        List<Pair<String, Integer>> board = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {

            int randomNumber = (int) (Math.random() * RANGE + 1);

            System.out.print("Sisesta oma nimi: ");
            String playerName = scanner.nextLine();

            int guessCount = handleGuessing(scanner, randomNumber);
            int userNumber;
            System.out.println("Õige vastus! Sul kulus " + guessCount + " korda, et ära arvata!");

            // tegeleme logikirje lisamisega...
            //Map<String, Integer> result = new HashMap<>(); // loome uue mapi
            Pair<String, Integer> result = new Pair<>(playerName, guessCount); // loome uue mapi
            // result.put(playerName, guessCount);
            board.add(result);

            if (handleFlow(board, scanner, playerName, guessCount)) {
                return;
            }
        }
    }

    private static boolean handleFlow(List<Pair<String, Integer>> board, Scanner scanner, String playerName, int guessCount) {
        int userNumber;
        boolean continueCycle = true;
        while (continueCycle) {
            System.out.println("Vali tegevus (1 - välju programmist, 2 - prindi logi,  muu number - uus mäng): ");
            userNumber = Integer.parseInt(scanner.nextLine());
            if (userNumber == 1) {
                return true;
            } else if (userNumber == 2) {
                printResults(board, playerName, guessCount);
            } else {
                continueCycle = false;

            }
        }
        return false;
    }

    private static void printResults(List<Pair<String, Integer>> board, String playerName, int guessCount) {
        System.out.println(board);
        System.out.println("-----------------------");
        System.out.println("TULEMUSED");
        System.out.println("-----------------------");
        for (int i = 0; i < board.size(); i++) {
            // System.out.println(board.get(i));
            System.out.println("Nimi: " + playerName);
            System.out.println("Kordi " + guessCount);
        }


        System.out.println("-------------------");
    }

    private static int handleGuessing(Scanner scanner, int randomNumber) {
        int userNumber = 0;
        int guessCount = 0;

        do {
            guessCount++;
            System.out.println("Sisesta suvaline number vahemikus 1 - 10: ");
            userNumber = Integer.parseInt(scanner.nextLine());
            if (userNumber > randomNumber) {
                System.out.println("Liiga suur!");
            } else if (userNumber < randomNumber) {
                System.out.println("Liiga väike");
            }

        } while (randomNumber != userNumber);
        return guessCount;
    }

    public static class MyPair {
        public String userName;
        public int guessCount;
    }
}
