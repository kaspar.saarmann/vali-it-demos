package week1.day04;

import org.w3c.dom.ls.LSOutput;

import javax.swing.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class Day04CollectionsEx {
    public static void main(String[] args) {

        //Ex 19 list
        List<String> cities = new ArrayList<>();
        cities.add("Tallinn");
        cities.add("Tartu");
        cities.add("Keila");
        cities.add("Narva");
        cities.add("Võru");

        System.out.println(cities.get(0) + ", " + cities.get(2) + ", " + cities.get(4));
        System.out.println("Esimene linn: " + cities.get(0));
        System.out.println("Kolmas linn: " + cities.get(2));
        System.out.println("Viies linn: " + cities.get(cities.size() - 1));

        //Ex 20 queue
        Queue<String> myQue = new LinkedList<>();
        myQue.add("Ahti");
        myQue.add("Mari");
        myQue.add("Juku");
        myQue.add("Mart");
        myQue.add("Mihkel");
        myQue.add("Ülo");

        //myQue.remove(); eemaldab listist esimese nime ja annab selle mulle
//        System.out.println(myQue.remove()); annab välja esimese
//        System.out.println(myQue.remove()); annab välja teise
//        System.out.println(myQue); näitab mis on alles

        while (!myQue.isEmpty()) {// kui ei ole empty tee järgmist
            System.out.println(myQue.remove());
        }

//        for(String name : myQue) {
//            System.out.println();

        //deque on double ended saab lugeda mõlemast otsast
//        Deque<Integer> stack = new ArrayDeque<>();
//        stack.add(1);
//        stack.add(2);
//        System.out.println(stack.removeLast());
//        System.out.println(stack.removeLast());

        //Ex 21
        Set<String> metsaloomad = new TreeSet<>();

        metsaloomad.add("Karu");
        metsaloomad.add("Jänes");
        metsaloomad.add("Rebane");
        metsaloomad.add("Siil");
        metsaloomad.add("Hunt");

        for(String tree : metsaloomad) {
            System.out.println(tree);

        }
            metsaloomad.forEach(x -> System.out.println(x));

        //Ex 22
        Map<String, String[]> countryMap = new HashMap<>();
        countryMap.put("Eesti", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countryMap.put("Rootsi", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countryMap.put("Soome", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsa"});

        //variant 1
        String[] countryNames = countryMap.keySet().toArray(String[]::new); // teeb keyseti massiiviks
        for (int i = 0; i < countryNames.length; i++){ // käib ükshaaval keyd läbi
            String[] currentCountryCities = countryMap.get(countryNames[i]);
            System.out.println("Country: " + countryNames[i]);
            System.out.println("Cities: ");
            for (int j = 0; j < currentCountryCities.length; j++){
                System.out.println("\t" + currentCountryCities[j]); // \t teeb taande
            }
        }

        //variant 2
        for (Map.Entry<String, String[]> country : countryMap.entrySet()){
            System.out.println("Country: " + country.getKey());
            System.out.println("Cities:");
            for(String city : country.getValue()){
                System.out.println("\t" + city);
            }
        }

         //variant 3
            countryMap.forEach((niptsada, linn) -> { //sulgudes suvalised sõnad
            System.out.println("Country " + niptsada);
            System.out.println("Cities:");
            Stream.of(linn).forEach(city -> System.out.println("\t" + city));
        });
    }
}
