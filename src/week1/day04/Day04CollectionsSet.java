package week1.day04;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Day04CollectionsSet {
    public static void main(String[] args) {
        Set<String> namesSet = new HashSet<>();

        namesSet.add("Rainer");
        namesSet.add("Rainer");
        namesSet.add("Rainer");
        namesSet.add("Adalbert");
        System.out.println(namesSet);

        // teeme setist massiivi
        String[] namesArray = namesSet.toArray(String[]::new); // teeb stringide massiivi
        Object[] namesArray1 = namesSet.toArray(Object[]::new); // teeb objectide massiivi

        Iterator<String> namesSetIterator = namesSet.iterator();// iterator on objekt mis aitab teostada tsüklilisi operatsioone
        while(namesSetIterator.hasNext()){
            System.out.println(namesSetIterator.next());
        }

        // setile ei pääse ligi suvalises kohas, tuleb loopiga kogu set läbi käia

    }
}
