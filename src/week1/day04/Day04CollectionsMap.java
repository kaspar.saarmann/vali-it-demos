package week1.day04;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Day04CollectionsMap {
    public static void main(String[] args) {

        Map<String, String> estEngDict = new HashMap<>();
        //Set<String> estonianWords = entEngDict.keySet();

        estEngDict.put("auto", "car");
        estEngDict.put("isik", "person");
        estEngDict.put("riik", "country");
        estEngDict.put("sõiduk", "car");
        System.out.println(estEngDict);
        System.out.println(estEngDict.keySet());
        System.out.println(estEngDict.values());

        //eemaldamine
        estEngDict.remove("auto");
        System.out.println(estEngDict);

        //asendamine
        estEngDict.replace("sõiduk", "machine");
        System.out.println(estEngDict);

        //mapi väljaprintimine
        for (String wordEst : estEngDict.keySet()) {
            System.out.println("Sõna " + wordEst + " on inglise keeles " + estEngDict.get(wordEst));

        }

        //trükkime välja vastupidi
        //estEngDict.entrySet() annab key=value ehk annab kõik paarid
        for (Map.Entry <String, String> words : estEngDict.entrySet() ) {
            System.out.println("Sõna " + words.getValue() + " on eesti keeles " + words.getKey());

        }
    }

}
