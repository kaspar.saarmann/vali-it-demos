package week1.Hello;

public class HelloWorld {

    public static void main(String[] args) { // main meetod peab olema public, et virtuaalmasin saaks sisse. Kui main meetod puudub, siis vähemalt üks klass peab olema public
        System.out.println("Hello World");
        System.out.println("Hello World, " + args [0] + "!");


    }

}
