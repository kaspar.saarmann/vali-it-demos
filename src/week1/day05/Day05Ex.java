package week1.day05;

import java.sql.PreparedStatement;
import java.util.*;

public class Day05Ex {
    public static void main(String[] args) {
        //Ex 1
        String pic = "######";

        for (int i = 0; i < pic.length(); i++) {
            String pic2 = pic.substring(0, pic.length() - i);
            System.out.println(pic2);
        }

        //Ex1 ver2
        int rows = 6;
        int cols = 6;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (row <= col) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        //Ex2
        int rows1 = 6;
        int cols1 = 6;
        for (int c = 0; c < rows1; c++) {
            for (int f = 0; f < cols1; f++) {
                if (c >= f) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        //Ex 3
        System.out.println();
        int rows2 = 5;
        int cols2 = 5;
        for (int row = 0; row < rows2; row++) {
            for (int col = 0; col < cols2; col++) {

                if (col < 4 - row) {
                    System.out.print(" ");

                }

            }

        }
        //Ex 4
        int x = 1234567;
        String numberStr = String.valueOf(x);
        String resultStr = "";
        for (char digit : numberStr.toCharArray()) {
            resultStr = digit + resultStr;
        }
        int result = Integer.parseInt(resultStr);
        System.out.println("Algne number " + x + ", ümberpööratud number " + resultStr);

        StringBuilder sb = new StringBuilder(String.valueOf(389));
        System.out.println(sb.reverse().toString());

        //Ex5

        for (int studentIndex = 0; studentIndex < args.length; studentIndex += 2) {

            int studentNameIndex = studentIndex;
            int studentScoreIndex = studentIndex + 1;

            String studentName = args[studentIndex];
            int examPoints = Integer.parseInt(args[studentScoreIndex]);
            int grade = 0;

            if (examPoints > 90) {
                grade = 5;
            } else if (examPoints > 80) {
                grade = 4;
            } else if (examPoints > 70) {
                grade = 3;
            } else if (examPoints > 60) {
                grade = 2;
            } else if (examPoints > 50) {
                grade = 1;
            }

            if (grade == 0) {
                System.out.printf("%s: FAIL\n", studentName);
            } else {
                System.out.printf("%s: PASS - %d. %d\n, ", studentName, grade, examPoints);
            }
        }

        //Ex 6
        double[][] triangleSides = {
                {3.0, 4.0},
                {12.3, 65.85},
                {134.23, 6.12},
                {42.02, 23.75},
                {33.52, 102.52},
                {5.54, 122.33}
        };
        for (double[] triangle : triangleSides) {
            double side1 = triangle[0];
            double side2 = triangle[1];
            double side3 = Math.sqrt((Math.pow(side1, 2) + Math.pow(side2, 2)));
            System.out.printf("Kolmnurgal külgedega %.2f ja %.2f on hüpotenuusi pikkus %.2f\n", side1, side2, side3);
        }

        //Ex7
        System.out.println();
        String[][] primeMinister = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Finland", "Helsinki", "Antti Rinne"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Spain", "Madrid", "Pedro Sánchez"},
                {"United Kingdom", "London", "Boris Johnson"},
                {"Italy", "Rome", "Giuseppe Conte"},
                {"Greece", "Athens", "Kyriákos Mitsotákis"}
        };
        System.out.println("Peaministrid");
        for (String[] minister : primeMinister) {
            System.out.println("\t" + minister[2]);

            System.out.println("Riigid");
            for (int i = 0; i < primeMinister.length; i++) {
                System.out.printf("\tRiik: %s, pealinn: %s, peaminister: %s\n", primeMinister[i][0], primeMinister[i][1], primeMinister[i][2]);
            }
        }

        //Ex8
        System.out.println();
        String[][][] primeMinister2 = {
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Eesti", "Vene", "Soome"}},
                {{"Finland"}, {"Helsinki"}, {"Antti Rinne"}, {"Soome", "Rootsi", "Vene"}},
                {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Rootsi", "Soome"}},
                {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Norra", "Rootsi"}},
                {{"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Leedu", "Läti", "Vene"}},
                {{"Spain"}, {"Madrid"}, {"Pedro Sánchez"}, {"Hispaania", "Portugali"}},
                {{"United Kingdom"}, {"London"}, {"Boris Johnson"}, {"Inglise", "Vene", "Eesti", "Poola"}},
                {{"Italy"}, {"Rome"}, {"Giusepp Conte"}, {"Itaalia", "Poola"}},
                {{"Greece"}, {"Athens"}, {"Kyriákos Mitsotákis"}, {"Kreeka"}}
        };
        System.out.println("Riigid");
        for (String[][] country : primeMinister2) {
            System.out.println("\t" + country[0][0] + ", " + country[1][0] + ", " + country[2][0]);
        }
        System.out.println("Riigid veelkord");
        for (int i = 0; i < primeMinister2.length; i++) {
            System.out.println("\t" + primeMinister2[i][0][0] + ", " + primeMinister2[i][1][0] + ", " + primeMinister2[i][2][0]);
        }
        System.out.println("Riigi keeled");
        for (String[][] country : primeMinister2) {
            System.out.println("\t" + country[0][0] + ", " + country[3][0]);
            for (String countryLanguage : country[3]) {
                System.out.println("\t\t" + countryLanguage);
            }
        }
        //Ex 8 variant 2
        System.out.println();
        Object[][] countries3 = {
                {"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Eesti", "Vene", "Soome"}},
                {"Finland", "Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi", "Vene"}},

        };
        for (int i = 0; i < countries3.length; i++) {
            System.out.println("\t" + countries3[i][0] + "," + countries3[i][1] + ", " + countries3[i][2]);
            String[] languages = (String[]) countries3[i][3]; // ütleme jõuga et countries3 on Stringide massiid - (String)
            for (String language : languages) {
                System.out.println("\t\t" + language);
            }
        }

        //Ex 9
        System.out.println("Ex9");
        List<Map<String, String[]>> countryList = new ArrayList<>();
        Map<String, String[]> estonia = new HashMap<>();
        estonia.put("name", new String[]{"Eesti"});
        estonia.put("capital", new String[]{"Tallinn"});
        estonia.put("Primeminister", new String[]{"Jüri Ratas"});
        estonia.put("languages", new String[]{"Eesti", "Vene", "Soome", "Läti"});
        countryList.add(estonia);

        Map<String, String[]> finland = new HashMap<>();
        finland.put("name", new String[]{"Soome"});
        finland.put("capital", new String[]{"Helsinki"});
        finland.put("Primeminister", new String[]{"Antti Rinne"});
        finland.put("languages", new String[]{"Vene", "Soome"});
        countryList.add(finland);

        for (int i = 0; i < countryList.size(); i++) {
            System.out.println(String.format("%s, %s, %s", countryList.get(i).get("name")[0], countryList.get(i).get("capital")[0], countryList.get(i).get("Primeminister")[0]));
            for (int j = 0; j < countryList.get(i).get("languages").length; j++) {
                System.out.println(countryList.get(i).get("languages")[j]);
            }
        }
        //Ex 10
        System.out.println();
        System.out.println("Ex10");

        Map<String, List<Object>> countries5 = new HashMap<>();

        //Eesti
        List<Object> estoniaItem = new ArrayList<>();
        estoniaItem.add("Tallinn");
        estoniaItem.add("Jüri Ratas");
        estoniaItem.add(new String[]{"Eesti", "Vene", "Soome", "Läti"});
        countries5.put("Eesti", estoniaItem);

        //Soome - teise meetodiga
        List<Object> finlandItem = Arrays.asList("Helsinki", "Antti Rinne", new String[]{"Soome", "Rootsi", "Eesti"});
        countries5.put("Soome", finlandItem);

        for (String country : countries5.keySet()) {
            System.out.println("\t" + country + ", " + countries5.get(country).get(0) + ", " + countries5.get(country).get(1));

            String[] languages = (String[]) countries5.get(country).get(2);
            for (String language : languages) {
                System.out.println("\t\t" + language);
            }
        }
        System.out.println();

        //Ex 11
        System.out.println();
        System.out.println("Ex 11");

        Queue<List<Object>> countriesQueue = new LinkedList<>();

        countriesQueue.add(Arrays.asList("Eesti", "Tallinn", "Jüri Ratas", new String [] {"Eesti", "Vene", "Poola"}));
        countriesQueue.add(Arrays.asList("Soome", "Helsinki", "Antti Rinne", new String [] {"Eesti", "Vene", "Soome", "Poola"}));

        while (!countriesQueue.isEmpty()){
            List<Object> countryInfo = countriesQueue.remove();

            System.out.printf("%s, %s, %s: \n", countryInfo.get(0), countryInfo.get(1), countryInfo.get(2));
            String[] languages = (String[]) countryInfo.get(3);

            for (int i = 0; i < languages.length; i++){
                System.out.println("\t" + languages[i]);
            }
        }
    }
}



