package week1.day02;

public class ExTrafficLight {
    public static void main(String[] args) {
        //Ex valgusfoor
//        String color = args[0].toUpperCase();
//
//        switch (color) {
//            case "RED":
//                System.out.println("Driver has to stop car and wait for green light");
//                break;
//            case "GREEN":
//                System.out.println("Driver can drive a car");
//                break;
//            case "YELLOW":
//                System.out.println("Driver has to be ready to stop the car or to start driving");
//                break;
//            default:
//                System.out.println("ERROR");
//        }

        String color = args[0].toUpperCase();

        if (color.equals("RED")){
            System.out.println("Driver has to stop car and wait for green light");
        } else if (color.equals("YELLOW")) {
            System.out.println("Driver has to be ready to stop the car or to start driving");
        } else if (color.equals("GREEN")) {
            System.out.println("Driver can drive a car");
        } else {
            System.out.println("ERROR");
        }
    }


}
