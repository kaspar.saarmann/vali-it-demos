package week1.day02;

import java.math.BigDecimal;

import static javafx.scene.input.KeyCode.L;

public class ExWeek01 {
    public static void main(String[] args) {
        //Ex 1
        double myDouble1 = 456.78;
        float myFloat2 = (float) 466.78;

        // Ex 2
        String mytext1 = "test";

        //Ex 3
        boolean myBoolean1 = true;

        //ex 4
        int[] myNumbers = {5, 91, 304, 405};
        myNumbers = new int[4];
        myNumbers[0] = 5;
        myNumbers[1] = 91;
        myNumbers[2] = 304;
        myNumbers[3] = 405;

        //ex 5
        double[] myDoubleNumbers2 = {56.7, 45.8, 91.2};

        //Ex6
        String[] myValues = {"see on esimene väärtus", "67", "58.92"};
        double myOtherList = Double.parseDouble(myValues[2]); //

        //Ex 7
        BigDecimal myBigDecimal1 = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345");
        //myBigDecimal1 = myBigDecimal1.add(new BigDecimal("1")); // liidab ühe numbrile juurde, tegemist on objektiga, seega ei saa lihtsalt 1 lisada, ainult funktsiooniga

        System.out.println(myBigDecimal1);



    }

}