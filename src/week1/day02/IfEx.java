package week1.day02;

public class IfEx {
    public static void main(String[] args) {
//        int x = Integer.parseInt(args[0]); // args sisestamisel on tegemist String vaja väärtust int
//        if (x % 2 == 0){
//            System.out.println("Arv " + x + " on paaris!");
//        } else {
//            System.out.println("Arv on paaritu!");


//        String outPutText = (x % 2 == 0) ? String.format("Arv %d on paaris ", x) : String.format("Arv %d on paaritu ", x);

//        System.out.println((x % 2 == 0) ? String.format("Arv %d on paaris ", x) : String.format("Arv %d on paaritu ", x));

        // Switch demo

//        switch (x) {
//            case 2:
//                System.out.println("Hinne oli keskpärane");
//                break;
//            case 3:
//                System.out.println("Hinne oli keskpärane");
//                break;
//            case 4:
//                System.out.println("Hinne oli keskpärane");
//                break;
//            case 5:
//                System.out.println("hinne oli täitsa hea");
//                break;
//            case 6:
//                System.out.println("hinne ületas ootusi");
//                break;
//            default:
//                System.out.println("hinne on tuvastamatu");

        //Ex 2: valikute konstrueerimine
//        String var = "Berlin";
//
//        if (var.equalsIgnoreCase("Milano")) { // ignore case ei pea oluliseks kas suur või väike täht
//            System.out.println("Ilm on soe");
//
//        } else
//            System.out.println("Ilm polegi kõige tähtsam");
//
//        //Ex 3: if, else if, else
//        int grade = Integer.parseInt(args[0]);
//
//        if (grade == 1){
//            System.out.println("nõrk");
//        } else if (grade == 2){
//            System.out.println("mitterahuldav");
//        } else if (grade == 3){
//            System.out.println("rahuldav");
//        } else if (grade == 4){
//            System.out.println("hea");
//        } else {
//            System.out.println("väga hea");
//        }
//
//        //Ex 4: switch
//        switch (grade){
//            case 1:
//                System.out.println("nõrk");
//                break;
//            case 2:
//                System.out.println("mitterahuldav");
//                break;
//            case 3:
//                System.out.println("rahuldav");
//                break;
//            case 4:
//                System.out.println("hea");
//                break;
//            case 5:
//                System.out.println("väga hea");
//                break;
//            default:
                
   //     }

        //Ex 5; inline-if
        int myVariable1 = 105;

        String group  = (myVariable1 > 100) ? "noor" : "vana";
        System.out.println("Vanusegrupp " + group);

        }
    }
