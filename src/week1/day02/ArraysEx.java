package week1.day02;

import java.util.Arrays;

public class ArraysEx {
    public static void main(String[] args) {
//        //Ex 7
//        int[] myIntArray;
//        myIntArray = new int[5]; // int[] myIntArray = new int [5]
//        myIntArray[0] = 1;
//        myIntArray[1] = 2;
//        myIntArray[2] = 3;
//        myIntArray[3] = 4;
//        myIntArray[4] = 5;
//
//       // 2 variant - myIntArray[] = new int [] {1, 2, 3, 4, 5}
//
//        System.out.println(myIntArray[0]);
//        System.out.println(myIntArray[2]);
//        System.out.println(myIntArray[4]);
//        //  System.out.println(myIntArray[myIntArray.length -1]) - prindib viimase väärtuse, kui ei tea kui pikk array on
//
//        //Ex8
//        String[] myStringArray = new String[] {"Tallinn", "Helsinki", "Madrid", "Paris"}; // see new String ei ole vajalik
//        System.out.println(Arrays.toString(myStringArray));
//
//        //prindi kogu string
//        //System.out.println(Arrays.toString(myStringArray));
//
//        // tsükliga
//        // for (String city : myStringArray){
//        // System.out.println(city);
//
//        //Ex 9
//        int[][] my2DArray = {
//                {1, 2, 3},
//                {4, 5, 6},
//                {7, 8, 9, 0}
//        };
//        for (int[] n : my2DArray){
//            for (int v : n) {
//                System.out.println(v);
//            }
//
////         for(int a = 0; a < my2DArray.length; a++){
////             for (int b = 0; b < my2DArray.length; b++){
////                 System.out.println(my2DArray[a][b]);
//             }
//
//
//        //Ex 10
//       //Variant 1
//        String[][] countryCities = {
//                {"Tallinn", "Tartu", "Valga", "Võru"},
//                {"Stockholm", "Uppsala", "Lund", "Köping"},
//                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
//        };
//        //Variant 2
////        String [][] countryCities2 = new String [3][4];
////        countryCities2[0][0] = "Tallinn";
////        countryCities2[0][1] = "Tartu";
////        countryCities2[0][2] = "Valga";
////        countryCities2[0][3] = "Võru";
////
////        countryCities2[1][0] = "Stockholm";
////        countryCities2[1][1] = "Uppsala";
////        countryCities2[1][2] = "Lund";
////        countryCities2[1][3] = "Köping";
////
////        countryCities2[2][0] = "Helsinki";
////        countryCities2[2][1] = "Espoo";
////        countryCities2[2][2] = "Hanko";
////        countryCities2[2][3] = "Jämsa";
//
//    }
//}


    }
}